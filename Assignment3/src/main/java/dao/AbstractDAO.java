package dao;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import models.*;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 * @Source http://www.java-blog.com/mapping-javaobjects-database-reflection-generics
 */
public class AbstractDAO<T> {
	protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

	private final Class<T> type;

	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	}

	public String createInsertStatement() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("Insert into ");
		sb.append(type.getSimpleName()+"(");
		
		for (Field field : type.getDeclaredFields())
		{
			if(!field.getName().equals("id"))
			sb.append(field.getName()+", ");
		}
		
		String s = sb.toString();
		s = s.substring(0, s.length()-2)+") values (";
		
		for (Field field : type.getDeclaredFields())
		{
			if(!field.getName().equals("id"))
			s = s + "?, ";
		}
		
		s = s.substring(0, s.length()-2)+")";
		System.out.println(s);
		return s;
	}
	
	public String createUpdateStatement() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("Update ");
		sb.append(type.getSimpleName()+" set ");
		
		for (Field field : type.getDeclaredFields())
		{
			if(!field.getName().equals("id"))
			sb.append(field.getName()+"=?, ");
		}
		
		String s = sb.toString();
		
		s = s.substring(0, s.length()-2) + " where id=?";
		System.out.println(s);
		return s;
	}
	
	public String createDeleteStatement()
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append("{call delete"+type.getSimpleName()+"(?)}");
		
		return sb.toString();
	}
	
	private String createSelectQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE " + field + " =?");
		return sb.toString();
	}

	public List<T> findAll() {
		
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = "Select * from "+type.getSimpleName();
		
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();

			return createObjects(resultSet);
		}catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}
	
	public T findById(int id) {
		
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("id");
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			return createObjects(resultSet).get(0);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	private List<T> createObjects(ResultSet resultSet) {
		
		List<T> list = new ArrayList<T>();

		try {
			while (resultSet.next()) {
				T instance = type.newInstance();
				for (Field field : type.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		return list;
	}

	public void insert(T t) {
		Connection connection = null;
		PreparedStatement statement = null;
		String query = this.createInsertStatement();
		int i=1;
		
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			
			for(Field f : t.getClass().getDeclaredFields())
			{
				if(i!=1)
				{
					f.setAccessible(true);
					try {
						statement.setObject(i-1, f.get(t));
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
				}
				i++;
			}
			
			statement.execute();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		
	}

	public void update(T t, int id) {
		
		Connection connection = null;
		PreparedStatement statement = null;
		String query = this.createUpdateStatement();
		
		int i=1;
		
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			
			for(Field f : t.getClass().getDeclaredFields())
			{
				if(i!=1)
				{
					f.setAccessible(true);
					try {
						statement.setObject(i-1, f.get(t));
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
				}
				i++;
			}
			
			statement.setInt(i-1, id);
			statement.execute();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
	}
	
public void deleteById(int id) {
		
		Connection connection = null;
		CallableStatement statement = null;
		String query = this.createDeleteStatement();
		
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareCall(query);
			statement.setInt(1, id);
			statement.execute();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
	}
}
