package Presentation;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import models.Customer;

public class ReflectionClass{

	public static <T> void setTableContent(DefaultTableModel model, List<T> obj)
	{
		Vector row = new Vector();
		ArrayList<Object> rows = new ArrayList<Object>();
		
		Object ob = obj.get(0);
		
		for(Field f : ob.getClass().getDeclaredFields())
		{
			model.addColumn(f.getName());
		}
		
		for(T o : obj)
		{
			rows.clear();
			for(Field f : o.getClass().getDeclaredFields())
			{
				f.setAccessible(true);
				try {
					rows.add(f.get(o));
					System.out.println(f.get(o));
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
			model.addRow(rows.toArray());
		}
		
	}
}
