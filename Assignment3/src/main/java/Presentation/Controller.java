package Presentation;

public class Controller {
	
	private MainFrame mainFrame;
	private ManageClientsFrame manageClientsFrame;
	private ManageProductsFrame manageProductsFrame;
	private PlaceOrderFrame placeOrderFrame;
	
	public Controller()
	{
		mainFrame = new MainFrame();
		mainFrame.setVisible(true);
		
		mainFrame.btnManageClientsAction(a->{
			
			manageClientsFrame = new ManageClientsFrame();
			manageClientsFrame.setVisible(true);
			
		});
		
		mainFrame.btnManageProductsAction(a->{
			
			manageProductsFrame = new ManageProductsFrame();
			manageProductsFrame.setVisible(true);
			
		});
		
		mainFrame.btnPlaceAnOrderAction(a->{
			
			placeOrderFrame = new PlaceOrderFrame();
			placeOrderFrame.setVisible(true);
		});
	}
	
}
