package Presentation;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import bll.CustomerBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import models.Customer;
import models.Orders;
import models.Product;

import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.List;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JButton;

public class PlaceOrderFrame extends JFrame {

	private JPanel contentPane;
	private JTable clientsTable = new JTable();
	private JTable productsTable = new JTable();
	private JTextField amountField = new JTextField();;
	private JScrollPane scrollPane = new JScrollPane();
	private JScrollPane scrollPane1 = new JScrollPane();
	private JLabel lblSelectAClient = new JLabel("Select a client:");
	private JLabel lblSelectAProduct = new JLabel("Select a product:");
	private JLabel lblOrderDetails = new JLabel("Order details:");
	private JLabel lblQuantity = new JLabel("Quantity:");
	private JButton btnPlaceOrder = new JButton("Place Order");
	private CustomerBLL cBLL = new CustomerBLL();
	private ProductBLL pBLL = new ProductBLL();
	private OrderBLL oBLL = new OrderBLL();
	private JOptionPane frame = new JOptionPane();

	public PlaceOrderFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 750, 565);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		scrollPane.setViewportView(clientsTable);
		scrollPane1.setViewportView(productsTable);
		
		lblSelectAClient.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblSelectAProduct.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblOrderDetails.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		amountField.setColumns(10);
		
		addComponentsToPane();
		setComponentsBounds();
		setClientsTableContent();
		setProductsTableContent();
		
		this.placeOrderButtonAction(a->{
			
			if(productsTable.getSelectedRow() != -1 && clientsTable.getSelectedRow() != -1)
			try{
				Customer c = cBLL.findCustomerById(Integer.parseInt((String) clientsTable.getModel().getValueAt(clientsTable.getSelectedRow(), 0)));
				Product p = pBLL.findProductById(Integer.parseInt((String) productsTable.getModel().getValueAt(productsTable.getSelectedRow(),0)));
				
				try {
					if(amountField.getText().equals(""))
					{
						;	
						
					}else{

					pBLL.decreaseStock(p.getId(), Integer.parseInt(amountField.getText()));
					oBLL.insert(new Orders(0, c.getId(), p.getId(), Integer.parseInt(amountField.getText())));
					setProductsTableContent();
					
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(frame,"Not enough products!");
				}	
				this.setProductsTableContent();
				this.printToFile("Note\n"+c.getName()+":\n"+p.getName()+"........"+Integer.parseInt(amountField.getText())+"\nTotal: "+Integer.parseInt(amountField.getText())*p.getPrice(), oBLL.getLastOrderId());
				JOptionPane.showMessageDialog(frame,"Thank you for buying from us!");
				
			}catch(Exception ex) {
				JOptionPane.showMessageDialog(frame,"Invalid quantity data!");
			}
			else
			{
				JOptionPane.showMessageDialog(frame,"Please select rows in both tables!");
			}
		});
		
	}
	
	public void placeOrderButtonAction( ActionListener a)
    {
		btnPlaceOrder.addActionListener(a);
	}
	
	public void setProductsTableContent()
	{
		DefaultTableModel model = new DefaultTableModel();
		List<Product> products = pBLL.findAll();
		
		ReflectionClass.setTableContent(model, products);
		
		productsTable.setModel(model);
	}
	
	public void setClientsTableContent()
	{
		List<Customer> customers = cBLL.findAll();
		DefaultTableModel model = new DefaultTableModel();
		
		ReflectionClass.setTableContent(model, customers);

		clientsTable.setModel(model);
	}
	
	void printToFile(String s, int orderId)
	{
		PrintWriter writer;
		try {
			writer = new PrintWriter("PaycheckOrderNo"+orderId+".rtf", "UTF-8");
			writer.println(s);
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	void addComponentsToPane()
	{
		contentPane.add(scrollPane);
		contentPane.add(scrollPane1);
		contentPane.add(lblSelectAClient);
		contentPane.add(lblSelectAProduct);
		contentPane.add(amountField);
		contentPane.add(lblOrderDetails);
		contentPane.add(lblQuantity);
		contentPane.add(btnPlaceOrder);
	}
	
	void setComponentsBounds()
	{
		scrollPane.setBounds(12, 34, 458, 216);
		scrollPane1.setBounds(12, 291, 458, 211);
		lblSelectAClient.setBounds(12, 13, 136, 16);
		lblSelectAProduct.setBounds(12, 262, 136, 16);
		amountField.setBounds(611, 243, 62, 22);
		lblOrderDetails.setBounds(549, 213, 136, 16);
		lblQuantity.setBounds(549, 246, 56, 16);
		btnPlaceOrder.setBounds(549, 278, 114, 25);
	}
	
}
