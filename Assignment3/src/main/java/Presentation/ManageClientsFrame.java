package Presentation;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.lang.reflect.Field;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import bll.CustomerBLL;
import models.Customer;

import java.awt.event.ActionEvent;

public class ManageClientsFrame extends JFrame {

	private JPanel contentPane = new JPanel();;
	private JTextField nameCreateField = new JTextField();
	private JTextField adressCreateField = new JTextField();
	private JTextField editNameField = new JTextField();
	private JTextField editAdressField = new JTextField();;
	private JTable table = new JTable();;
	private JLabel lblAddNewClient = new JLabel("Add New Client:");
	private JLabel lblName = new JLabel("Name:");
	private JLabel lblAdress = new JLabel("Adress:");
	private JLabel separator1 = new JLabel("---------------------------------------------------------------------------------------");
	private JLabel separator2 = new JLabel("---------------------------------------------------------------------------------------");
	private JLabel separator3 = new JLabel("---------------------------------------------------------------------------------------");
	private JButton addClientButton = new JButton("Add");
	private JLabel lblEditClient = new JLabel("Edit Client:");
	private JComboBox selectEdit = new JComboBox();
	private JLabel lblDeleteClient = new JLabel("Delete Client:");
	private JLabel newNameLabel = new JLabel("Name:");
	private JLabel newAdressLabel = new JLabel("Adress:");
	private JButton btnDone = new JButton("Done");
	private JComboBox selectDelete = new JComboBox();
	private JButton btnDelete = new JButton("Delete");
	private JLabel labelView = new JLabel("View All Clients:");
	private JScrollPane scrollPane = new JScrollPane();
	private JOptionPane frame = new JOptionPane();
	boolean flagUpdate = true;
	
	CustomerBLL cBLL = new CustomerBLL();
	
	public ManageClientsFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 666, 672);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		setComponentsBounds();
		addComponentsToPane();
		this.setTableContent();
		
		lblAddNewClient.setFont(new Font("Tahoma", Font.BOLD, 14));
		separator1.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblEditClient.setFont(new Font("Tahoma", Font.BOLD, 14));
		separator2.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblDeleteClient.setFont(new Font("Tahoma", Font.BOLD, 14));
		separator3.setFont(new Font("Tahoma", Font.BOLD, 16));
		labelView.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		nameCreateField.setColumns(10);
		adressCreateField.setColumns(10);
		editNameField.setColumns(10);
		editAdressField.setColumns(10);
	
		scrollPane.setViewportView(table);
		
		JLabel labelIdEdit = new JLabel("Id:");
		labelIdEdit.setBounds(164, 104, 56, 16);
		contentPane.add(labelIdEdit);
		
		JLabel label = new JLabel("Id:");
		label.setBounds(179, 197, 56, 16);
		contentPane.add(label);
		
		this.setExistingClients((ArrayList)cBLL.getAllIds());
		
		//BUTTONS
		
		this.addClientButtonAction(a->{
			
			if(this.getNewClientAdress().equals("") || this.getNewClientName().equals(""))
			{
				JOptionPane.showMessageDialog(frame,"Invalid data!");
			}
			else
			{
				cBLL.insertCustomer(new Customer(0, getNewClientName(), getNewClientAdress()));
				flagUpdate = false;
				this.setExistingClients((ArrayList)cBLL.getAllIds());
				flagUpdate = true;
				this.setTableContent();
				JOptionPane.showMessageDialog(frame,"Succes!");
			}
			
		});
	
		this.editSelectAction(a->{
			
			if(flagUpdate == true)
			if(!selectEdit.getSelectedItem().equals("Select ID"))
			{Customer c = cBLL.findCustomerById(Integer.parseInt((String) selectEdit.getSelectedItem()));
			editNameField.setText(c.getName());
			editAdressField.setText(c.getAdress());}
			else {
				editNameField.setText("");
				editAdressField.setText("");
			}
			
		});
		
		this.editButtonAction(a->{
			
			if(this.getClientAdress().equals("") || this.getClientName().equals(""))
			{
				JOptionPane.showMessageDialog(frame,"Invalid data!");
			}
			else
			{
				cBLL.updateCustomer(new Customer(0, getClientName(), getClientAdress()), Integer.parseInt((String) selectEdit.getSelectedItem()));
				this.setTableContent();
				JOptionPane.showMessageDialog(frame,"Succes!");
			}
		});
		
		this.deleteButtonAction(a->{
			if(selectDelete.getSelectedItem().equals("Select ID"))
			{
				JOptionPane.showMessageDialog(frame,"Please select an ID!");
			}
			else
			{
				cBLL.deleteByID(Integer.parseInt((String)selectDelete.getSelectedItem()));
				flagUpdate = false;
				this.setExistingClients((ArrayList)cBLL.getAllIds());
				flagUpdate = true;
				this.setTableContent();
				JOptionPane.showMessageDialog(frame,"Succes!");
			}
		});
	}
	
	public String getNewClientName()
	{
		return nameCreateField.getText();
	}
	
	public String getNewClientAdress()
	{
		return adressCreateField.getText();
	}
	
	public String getClientName()
	{
		return editNameField.getText();
	}
	
	public String getClientAdress()
	{
		return editAdressField.getText();
	}
	
	public void setExistingClients(ArrayList<String> names)
	{
		selectEdit.removeAllItems();
		selectDelete.removeAllItems();
		selectEdit.addItem("Select ID");
		selectDelete.addItem("Select ID");
		
		for(String s : names)
		{
			selectEdit.addItem(s);
			selectDelete.addItem(s);
		}
	}
	
	public String getSelectedClientEdit()
	{
		return (String) selectEdit.getSelectedItem();
	}
	
	public String getSelectedClientDelete()
	{
		return (String) selectDelete.getSelectedItem();
	}
	
	public void setTableContent()
	{
		List<Customer> customers = cBLL.findAll();
		DefaultTableModel model = new DefaultTableModel();
		
		ReflectionClass.setTableContent(model, customers);

		table.setModel(model);
	}
	
	public void addClientButtonAction( ActionListener a)
    {
		addClientButton.addActionListener(a);
	}
	
	public void editButtonAction( ActionListener a)
    {
		btnDone.addActionListener(a);
	}
	
	public void deleteButtonAction( ActionListener a)
    {
		btnDelete.addActionListener(a);
	}
	
	public void editSelectAction( ActionListener a)
	{
		selectEdit.addActionListener(a);
	}
	
	private void setComponentsBounds()
	{
		lblAddNewClient.setBounds(12, 35, 136, 16);
		lblName.setBounds(209, 13, 56, 16);
		lblAdress.setBounds(367, 13, 56, 16);
		nameCreateField.setBounds(170, 33, 116, 22);
		adressCreateField.setBounds(329, 33, 116, 22);
		separator1.setBounds(12, 75, 613, 16);
		separator2.setBounds(12, 173, 613, 16);
		separator3.setBounds(12, 263, 613, 16);
		addClientButton.setBounds(474, 32, 151, 25);
		lblEditClient.setBounds(12, 126, 90, 16);
		selectEdit.setBounds(114, 124, 121, 22);
		newNameLabel.setBounds(309, 104, 56, 16);
		editNameField.setBounds(270, 124, 116, 22);
		newAdressLabel.setBounds(451, 104, 56, 16);
		editAdressField.setBounds(413, 124, 116, 22);
		btnDone.setBounds(562, 123, 63, 25);
		selectDelete.setBounds(130, 216, 121, 22);
		lblDeleteClient.setBounds(12, 218, 106, 16);
		btnDelete.setBounds(270, 215, 97, 25);
		labelView.setBounds(244, 300, 136, 16);
		scrollPane.setBounds(12, 346, 624, 266);
	}
	
	private void addComponentsToPane()
	{
		contentPane.add(lblAddNewClient);
		contentPane.add(lblName);
		contentPane.add(lblAdress);		
		contentPane.add(nameCreateField);
		contentPane.add(adressCreateField);
		contentPane.add(separator1);
		contentPane.add(addClientButton);
		contentPane.add(lblEditClient);
		contentPane.add(selectEdit);
		contentPane.add(newNameLabel);
		contentPane.add(editNameField);
		contentPane.add(newAdressLabel);
		contentPane.add(editAdressField);
		contentPane.add(btnDone);
		contentPane.add(separator2);
		contentPane.add(lblDeleteClient);
		contentPane.add(selectDelete);
		contentPane.add(btnDelete);
		contentPane.add(separator3);
		contentPane.add(labelView);
		contentPane.add(scrollPane);
	}
}
