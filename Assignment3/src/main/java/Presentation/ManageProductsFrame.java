package Presentation;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import bll.ProductBLL;
import models.Customer;
import models.Product;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class ManageProductsFrame extends JFrame {

	private JPanel contentPane = new JPanel();
	private JPanel contentPane_1;
	private JTextField nameAddField = new JTextField();
	private JTextField priceAddField = new JTextField();
	private JTextField quantityAddField = new JTextField();
	private JTextField quantityEditField = new JTextField();
	private JTextField priceEditField = new JTextField();
	private JTextField nameEditField = new JTextField();
	private JTable table = new JTable();
	private JLabel lblAddNewProduct = new JLabel("Add New Product:");
	private JLabel lablNameAdd = new JLabel("Name:");
	private JLabel lblPriceAdd = new JLabel("Price:");
	private JButton btnAdd = new JButton("Add");
	private JLabel separator1 = new JLabel("---------------------------------------------------------------------------------------");
	private JButton btnDone = new JButton("Done");
	private JComboBox selectEdit = new JComboBox();
	private JLabel lblEditProduct = new JLabel("Edit Product:");
	private JLabel separator2 = new JLabel("---------------------------------------------------------------------------------------");
	private JLabel lblDeleteProduct = new JLabel("Delete Product:");
	private JComboBox selectDelete = new JComboBox();
	private JButton btnDelete = new JButton("Delete");
	private JLabel separator3 = new JLabel("---------------------------------------------------------------------------------------");
	private JLabel lblViewAllProducts = new JLabel("View All Products:");
	private JScrollPane scrollPane = new JScrollPane();
	private JLabel lblQuantityAdd = new JLabel("Quantity:");
	private JLabel lblQuantityEdit = new JLabel("Quantity:");
	private JLabel lblPriceEdit = new JLabel("Price:");
	private JLabel lblNameEdit = new JLabel("Name:");
	private final JLabel labelIdEdit = new JLabel("Id:");
	private final JLabel labelIdDelete = new JLabel("Id:");
	private ProductBLL pBLL = new ProductBLL();
	private JOptionPane frame = new JOptionPane();
	private boolean flagUpdate = true;
	
	public ManageProductsFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 667, 671);
		contentPane_1 = new JPanel();
		contentPane_1.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane_1);
		contentPane_1.setLayout(null);
		
		lblViewAllProducts.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblAddNewProduct.setFont(new Font("Tahoma", Font.BOLD, 14));
		separator1.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblEditProduct.setFont(new Font("Tahoma", Font.BOLD, 14));
		separator2.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblDeleteProduct.setFont(new Font("Tahoma", Font.BOLD, 14));
		separator3.setFont(new Font("Tahoma", Font.BOLD, 16));
		nameAddField = new JTextField();
		
		nameAddField.setColumns(10);
		priceAddField.setColumns(10);
		quantityAddField.setColumns(10);
		quantityEditField.setColumns(10);
		priceEditField.setColumns(10);
		nameEditField.setColumns(10);
		
		scrollPane.setViewportView(table);
		
		setComponentsBounds();
		addComponentsToPane();
		
		this.setTableContent();
		this.setExistingProducts((ArrayList)pBLL.getAllIds());
		
		this.addProductButtonAction(a->{
			
			if(this.getNewProductQuantity().equals("") || this.getNewProductName().equals("") || this.getNewProductPrice().equals(""))
			{
				JOptionPane.showMessageDialog(frame,"Invalid data!");
			}
			else
			{
				try {
					pBLL.insertProduct(new Product(0, getNewProductName(), Float.parseFloat(getNewProductPrice()), Integer.parseInt(getNewProductQuantity())));
					flagUpdate = false;
					this.setExistingProducts((ArrayList)pBLL.getAllIds());
					flagUpdate = true;
					this.setTableContent();
					JOptionPane.showMessageDialog(frame,"Succes!");
				}catch(Exception ex)
				{
					JOptionPane.showMessageDialog(frame,"Invalid data!");
				}
			}
			
		});
		
		this.editSelectAction(a->{
			
			if(flagUpdate == true)
			if(!selectEdit.getSelectedItem().equals("Select ID"))
			{
				Product p = pBLL.findProductById(Integer.parseInt((String) selectEdit.getSelectedItem()));
				nameEditField.setText(p.getName());
				priceEditField.setText(Float.toString(p.getPrice()));
				quantityEditField.setText(Integer.toString(p.getQuantity()));
			}
			else {
				nameEditField.setText("");
				priceEditField.setText("");
				quantityEditField.setText("");
			}
			
		});
		
		this.editButtonAction(a->{
			
			if(this.getProductName().equals("") || this.getProductPrice().equals("") || this.getProductQuantity().equals(""))
			{
				JOptionPane.showMessageDialog(frame,"Invalid data!");
			}
			else
			{
				try {	
					pBLL.updateProduct(new Product(0, getProductName(), Float.parseFloat(getProductPrice()), Integer.parseInt(getProductQuantity())), Integer.parseInt((String) selectEdit.getSelectedItem()));
					this.setTableContent();
					JOptionPane.showMessageDialog(frame,"Succes!");
				}catch(Exception ex) {
					JOptionPane.showMessageDialog(frame,"Invalid data!");
				}
			}
		});
		
		this.deleteButtonAction(a->{
			if(selectDelete.getSelectedItem().equals("Select ID"))
			{
				JOptionPane.showMessageDialog(frame,"Please select an ID!");
			}
			else
			{
				pBLL.deleteByID(Integer.parseInt((String)selectDelete.getSelectedItem()));
				flagUpdate = false;
				this.setExistingProducts((ArrayList)pBLL.getAllIds());
				flagUpdate = true;
				this.setTableContent();
				JOptionPane.showMessageDialog(frame,"Succes!");
			}
		});
		
	}
	
	public String getNewProductName()
	{
		return nameAddField.getText();
	}
	
	public String getNewProductPrice() 
	{
		return priceAddField.getText();
	}
	
	public String getNewProductQuantity() 
	{
		return quantityAddField.getText();
	}
	
	public String getProductName()
	{
		return nameEditField.getText();
	}
	
	public String getProductPrice() 
	{
		return priceEditField.getText();
	}
	
	public String getProductQuantity()
	{
		return quantityEditField.getText();
	}
	
	public void setExistingProducts(ArrayList<String> names)
	{
		selectEdit.removeAllItems();
		selectDelete.removeAllItems();
		selectEdit.addItem("Select ID");
		selectDelete.addItem("Select ID");
		
		for(String s : names)
		{
			selectEdit.addItem(s);
			selectDelete.addItem(s);
		}
	}
	
	public String getSelectedProductEdit()
	{
		return (String) selectEdit.getSelectedItem();
	}
	
	public String getSelectedProductDelete()
	{
		return (String) selectDelete.getSelectedItem();
	}
	
	public void setTableContent()
	{
		DefaultTableModel model = new DefaultTableModel();
		List<Product> products = pBLL.findAll();
		
		ReflectionClass.setTableContent(model, products);
		
		table.setModel(model);
	}
	
	public void addProductButtonAction( ActionListener a)
    {
		btnAdd.addActionListener(a);
	}
	
	public void editButtonAction( ActionListener a)
    {
		btnDone.addActionListener(a);
	}
	
	public void deleteButtonAction( ActionListener a)
    {
		btnDelete.addActionListener(a);
	}
	
	public void editSelectAction( ActionListener a)
	{
		selectEdit.addActionListener(a);
	}
	
	private void setComponentsBounds()
	{
		lblAddNewProduct.setBounds(12, 35, 136, 16);
		nameAddField.setBounds(170, 33, 87, 22);
		lablNameAdd.setBounds(191, 13, 56, 16);
		priceAddField.setBounds(290, 33, 69, 22);
		lblPriceAdd.setBounds(309, 13, 56, 16);
		btnAdd.setBounds(474, 32, 151, 25);
		separator1.setBounds(12, 75, 613, 16);
		btnDone.setBounds(562, 123, 63, 25);
		selectEdit.setBounds(114, 124, 106, 22);
		lblEditProduct.setBounds(12, 126, 106, 16);
		separator2.setBounds(12, 173, 613, 16);
		lblDeleteProduct.setBounds(12, 218, 116, 16);
		selectDelete.setBounds(130, 216, 106, 22);
		btnDelete.setBounds(270, 215, 97, 25);
		separator3.setBounds(12, 263, 613, 16);
		lblViewAllProducts.setBounds(222, 304, 136, 16);
		scrollPane.setBounds(12, 346, 624, 266);
		lblQuantityAdd.setBounds(400, 13, 56, 16);
		quantityAddField.setBounds(390, 33, 69, 22);
		quantityEditField.setBounds(467, 126, 69, 22);
		lblQuantityEdit.setBounds(477, 106, 56, 16);
		priceEditField.setBounds(367, 126, 69, 22);
		lblPriceEdit.setBounds(386, 106, 56, 16);
		nameEditField.setBounds(247, 126, 87, 22);
		lblNameEdit.setBounds(268, 106, 56, 16);
	}
	
	private void addComponentsToPane()
	{
			contentPane_1.add(lblAddNewProduct);	
			contentPane_1.add(nameAddField);	
			contentPane_1.add(lablNameAdd);	
			contentPane_1.add(priceAddField);	
			contentPane_1.add(lblPriceAdd);	
			contentPane_1.add(btnAdd);	
			contentPane_1.add(separator1);	
			contentPane_1.add(btnDone);	
			contentPane_1.add(selectEdit);	
			contentPane_1.add(lblEditProduct);	
			contentPane_1.add(separator2);	
			contentPane_1.add(lblDeleteProduct);	
			contentPane_1.add(selectDelete);	
			contentPane_1.add(btnDelete);	
			contentPane_1.add(separator3);
			contentPane_1.add(lblViewAllProducts);	
			contentPane_1.add(scrollPane);	
			contentPane_1.add(lblQuantityAdd);	
			contentPane_1.add(quantityAddField);	
			contentPane_1.add(quantityEditField);	
			contentPane_1.add(lblQuantityEdit);	
			contentPane_1.add(priceEditField);	
			contentPane_1.add(lblPriceEdit);	
			contentPane_1.add(nameEditField);	
			contentPane_1.add(lblNameEdit);
			labelIdDelete.setBounds(170, 197, 56, 16);
			
			contentPane_1.add(labelIdDelete);
			labelIdEdit.setBounds(153, 106, 56, 16);
			
			contentPane_1.add(labelIdEdit);
	}
}
