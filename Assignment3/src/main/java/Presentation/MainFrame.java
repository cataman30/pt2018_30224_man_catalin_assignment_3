package Presentation;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;

public class MainFrame extends JFrame {

	private JPanel contentPane;
	JLabel lblChooseTheDesired = new JLabel("Choose the desired operation: ");
	JButton btnManageClients = new JButton("Manage Clients");
	JButton btnManageProducts = new JButton("Manage Products");
	JButton btnPlaceAnOrder = new JButton("Place An Order");

	public MainFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 562, 438);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblChooseTheDesired.setBounds(175, 37, 282, 16);
		contentPane.add(lblChooseTheDesired);
		
		btnManageClients.setBounds(68, 98, 403, 67);
		contentPane.add(btnManageClients);
		
		btnManageProducts.setBounds(68, 189, 403, 67);
		contentPane.add(btnManageProducts);
		
		btnPlaceAnOrder.setBounds(68, 281, 403, 67);
		contentPane.add(btnPlaceAnOrder);
	}
	
	public void btnManageClientsAction( ActionListener a)
    {
		btnManageClients.addActionListener(a);
	}
	
	public void btnManageProductsAction( ActionListener a)
    {
		btnManageProducts.addActionListener(a);
	}
	
	public void btnPlaceAnOrderAction( ActionListener a)
    {
		btnPlaceAnOrder.addActionListener(a);
	}
}
