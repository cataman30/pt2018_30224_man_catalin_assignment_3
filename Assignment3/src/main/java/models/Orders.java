package models;

public class Orders {
	
	private int id;
	private int idCustomer;
	private int idProduct;
	private int quantity;
	
	public Orders(int id, int idCustomer, int idProduct, int quantity) {
		super();
		this.id = id;
		this.idCustomer = idCustomer;
		this.idProduct = idProduct;
		this.quantity = quantity;
	}
	
	public Orders()
	{
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(int idCustomer) {
		this.idCustomer = idCustomer;
	}

	public int getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	
}
