package bll;

import java.util.NoSuchElementException;

import dao.OrderDAO;
import models.Customer;
import models.Orders;

public class OrderBLL {

	private OrderDAO orderDAO = new OrderDAO();
	
	public void insert(Orders o)
	{
		orderDAO.insert(o);
	}
	
	public Orders findOrderById(int id) {
		Orders c = orderDAO.findById(id);
		if (c == null) {
			throw new NoSuchElementException("The student with id =" + id + " was not found!");
		}
		return c;
	}
	
	public int getLastOrderId()
	{
		return orderDAO.findAll().get(orderDAO.findAll().size()-1).getId();
	}
}
