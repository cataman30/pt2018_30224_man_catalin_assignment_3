package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import dao.ProductDAO;
import models.Orders;
import models.Product;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class ProductBLL {

	private ProductDAO productDAO;
	private Exception exception;

	public ProductBLL() {

		productDAO = new ProductDAO();
	}

	public Product findProductById(int id) {
		Product c = productDAO.findById(id);
		if (c == null) {
			throw new NoSuchElementException("The student with id =" + id + " was not found!");
		}
		return c;
	}
	
	public List<String> getAllIds()
	{
		List<Product> list = productDAO.findAll();
		List<String> stringList = new ArrayList<String>();
		
		if(!list.isEmpty())
		{
			for(Product c : list)
			{
			stringList.add(Integer.toString(c.getId()));
			}
		
			return stringList;
		}
		else
			return null;
	}
	
	public List<Product> findAll()
	{
		return productDAO.findAll();
	}
	
	public void insertProduct(Product c)
	{
		productDAO.insert(c);
	}
	
	public void updateProduct(Product p, int id)
	{
		productDAO.update(p, id);
	}

	public void deleteByID(int id)
	{
		productDAO.deleteById(id);
	}
	
	public int getStockById(int id)
	{
		Product p = productDAO.findById(id);
		
		return p.getQuantity();
	}
	
	public void decreaseStock(int id, int amount) throws Exception
	{
		Product p = productDAO.findById(id);
		
		if(amount<=getStockById(id))
			{
				p.setQuantity(p.getQuantity()-amount);
				productDAO.update(p, id);
			}
		else
			throw exception;
	}
}
