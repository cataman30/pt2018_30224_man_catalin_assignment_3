package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import dao.CustomerDAO;
import models.Customer;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class CustomerBLL {

	private CustomerDAO customerDAO;

	public CustomerBLL() {

		customerDAO = new CustomerDAO();
	}

	public Customer findCustomerById(int id) {
		Customer c = customerDAO.findById(id);
		if (c == null) {
			throw new NoSuchElementException("The student with id =" + id + " was not found!");
		}
		return c;
	}
	
	public List<String> getAllIds()
	{
		List<Customer> list = customerDAO.findAll();
		List<String> stringList = new ArrayList<String>();
		
		if(!list.isEmpty())
		{
			for(Customer c : list)
			{
			stringList.add(Integer.toString(c.getId()));
			}
		
			return stringList;
		}
		else
			return null;
	}
	
	public List<Customer> findAll()
	{
		return customerDAO.findAll();
	}
	
	public void insertCustomer(Customer c)
	{
		customerDAO.insert(c);
	}
	
	public void updateCustomer(Customer c, int id)
	{
		customerDAO.update(c, id);
	}

	public void deleteByID(int id)
	{
		customerDAO.deleteById(id);
	}
}
